# Flights

## Description
Program will print out all flight connection from origin to destination, where dataset of all available flights is stored in csv file.\
In one connection is never one airport visited more then once.\
The layover time cannot be less then 1 hour and cannot exceeds 6 hours\
The datasets have following columns:
- `flight_no`: Flight number.
- `origin`, `destination`: Airport codes.
- `departure`, `arrival`: Dates and times of the departures/arrivals.
- `base_price`, `bag_price`: Prices of the ticket and one piece of baggage.
- `bags_allowed`: Number of allowed pieces of baggage for the flight.

This python script takes 3 positional arguments
#### Positional Arguments
| Argument name | type    | Description              | Notes                        |
|---------------|---------|--------------------------|------------------------------|
| `input_file`  | string  | provided dataset of flights| May also be directory where are stored datasets with available flights, in this case the script will load data from every csv |
| `origin`      | string  | Origin airport code      |                              |
| `destination` | string  | Destination airport code |                              |

Also this python scipt provides few optional arguments
#### Optional Arguments

| Argument name  | type    | Description              | Notes                        |
|----------------|---------|--------------------------|------------------------------|
| `bags_count`   | integer | Number of requested bags | Optional (defaults to 0)     |
| `return_flight`| boolean | Is it a return flight?   | Optional (defaults to false) |
| `from_time`    | string  | Search for flights from given date. Date should be in ISO format| Optional(defaults to None) |
| `until_time`   | string  | Search for flights until given date. Date should be in ISO format| Optional(defaults to None) |
| `days`         | integer | Automatically search for return flights. Displayed connections enable you to stay given number of days in destination | Optional (defaults to None)|
| `output`       | string  | Will save output string to given output file | Optional (defaults to None ) |

#### Output
The output will be a json-compatible structured list of trips sorted by price. The trip has the following schema:

| Field          | Description                                                   |
|----------------|---------------------------------------------------------------|
| `flights`      | A list of flights in the trip according to the input dataset. |
| `origin`       | Origin airport of the trip.                                   |
| `destination`  | The final destination of the trip.                            |
| `bags_allowed` | The number of allowed bags for the trip.                      |
| `bags_count`   | The searched number of bags.                                  |
| `total_price`  | The total price for the trip.                                 |
| `travel_time`  | The total travel time.                                        |

#### Requirements
- `python3.4` or higher
- all imported modules are already built-in python, therefore no further instalment necessary
- no environment setup requirements

## Example Behaviour
For standart run with no additional arguments, we can use write into teminal:
```bash
python3 example0.csv WIW RFZ
```
If we want to specify, that we search only for connection in date from 1.1.2021 until 1.31.2021 with 3 bags on 5 days then:
```bash
python3 data.csv ORIGIN DEST --from 2021-01-01 --until 2021-01-31T23-59-59 -b 3 -d 5
```
or
```bash
python3 example0.csv WIW RFZ -r -f 2021-01-01 -u 2021-01-31T23-59-59 --bags_count 3 -days 5
```
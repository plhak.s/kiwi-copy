from __future__ import annotations
from typing import Dict, List, Union, Optional, Set
from datetime import datetime, timedelta
from pathlib import Path
import os.path
import csv
import json
import argparse

Connection = Dict[str, Union[str, float, int]]

HOUR_IN_SECONDS = 3600


class Flight:
    def __init__(self, stats: Dict[str, str]):
        self.flight_no = stats['flight_no']
        self.origin = stats['origin']
        self.destination = stats['destination']
        self.departure = stats['departure']
        self.arrival = stats['arrival']
        self.base_price = stats['base_price']
        self.bag_price = stats['bag_price']
        self.bags_allowed = stats['bags_allowed']


class Data:
    def __init__(self, input_file):
        self.input_file = input_file
        self.flights = {}

    def _load_data(self) -> bool:
        if os.path.isfile(self.input_file):
            self._load_file(self.input_file)
            return True

        if os.path.isdir(self.input_file):
            paths = Path(self.input_file).rglob('*.csv')
            total_valid_lines = 0
            for file in paths:
                total_valid_lines += self._load_file(str(file))
            print(f'total lines loaded: {total_valid_lines}')
            return True

        print(f'file {self.input_file} is not regular file nor directory')
        return False

    def _load_file(self, csv_file: str) -> int:
        num_invalid_rows = 0
        num_valid_lines = 0

        with open(csv_file, 'r', newline='') as file:
            reader = csv.DictReader(file)

            for row in reader:
                if not self._check_row(row):
                    num_invalid_rows += 1
                if row['origin'] not in self.flights:
                    self.flights[row['origin']] = []
                self.flights[row['origin']].append(Flight(row))
                # if self.check_flight_parameters(row):
                #     self.flights[row['origin']].append(Flight(row))
            # if self.print_load_message:
            #     num_valid_lines = reader.line_num - num_invalid_rows
            #     print(f'{csv_file}:\n'
            #           f'    valid lines: {num_valid_lines}\n'
            #           f'    invalid lines: {num_invalid_rows}')

        return num_valid_lines

    # def check_flight_parameters(self, row):
    #     if self.bags_count > row['bags_allowed']:
    #         return False
    #     if self.from_time is not None \
    #             and convert_iso_to_unix(self.from_time) > row['departure']:
    #         return False
    #     if self.until_time is not None and \
    #             convert_iso_to_unix(self.until_time) < row['arrival']:
    #         return False
    #     return True

    @staticmethod
    def _check_row(row):
        # check if row is in valid format and transform row to format
        # which is acceptable by class Flight
        arguments = [
            'flight_no',
            'origin',
            'destination',
            'departure',
            'arrival',
            'base_price',
            'bag_price',
            'bags_allowed'
        ]

        for arg in arguments:
            if arg not in row:
                print(f'input file is in invalid format - missing column {arg}')
                return False
            row[arg] = row[arg].upper()

        try:
            row['base_price'] = float(row['base_price'])
            row['bag_price'] = float(row['bag_price'])
            row['bags_allowed'] = int(row['bags_allowed'])
        except ValueError:
            print('base_price and bag_price in input file should be in float format')
            print('bags_allowed should be in integer format')
            return False

        return True

    def get_flights(self):
        self._load_data()
        return self.flights


class Search:
    def __init__(self, flights: Dict[str, List[Flight]], origin: str, destination: str,
                 bags_count: int, return_flight: bool, output: Optional[str],
                 from_time: Optional[str], until_time: Optional[str],
                 days: Optional[int], load_message: bool) -> None:

        self.origin = origin
        self.destination = destination
        self.bags_count = bags_count
        self.is_return = return_flight or days is not None
        self.output_file = output
        self.from_time = from_time
        self.until_time = until_time
        self.length_of_stay = days
        self.print_load_message = load_message
        self.flights = flights
        self.nodes: Dict[str, Node] = {}
        self.connections = []

    def get_stay_length(self):
        if self.length_of_stay is None:
            return HOUR_IN_SECONDS, float('inf')
        min_secs = self.length_of_stay * 24 * HOUR_IN_SECONDS
        max_secs = (self.length_of_stay + 1) * 24 * HOUR_IN_SECONDS
        return min_secs, max_secs

    def search_return_flights(self):
        assert self.is_return

        self.get_all_connections()
        connections = self.connections

        self.origin, self.destination = self.destination, self.origin
        self.nodes = {}
        self.connections = []

        self.get_all_connections()
        return_connections = self.connections

        self.connections = []

        for connection in connections:
            arrival_time = convert_iso_to_unix(connection['flights'][0]['arrival'])
            for return_connection in return_connections:
                departure_time = convert_iso_to_unix(return_connection['flights'][0]['departure'])
                min_secs, max_secs = self.get_stay_length()
                if arrival_time + max_secs >= departure_time >= arrival_time + min_secs:

                    total_price = connection['total_price'] + return_connection['total_price']
                    travel_time_secs = convert_time_to_seconds(connection['travel_time']) +\
                                       convert_time_to_seconds(return_connection['travel_time'])
                    travel_time = convert_seconds_to_time(travel_time_secs)

                    joined = {'flights': connection['flights'] + return_connection['flights'],
                              'bags_allowed': min(connection['bags_allowed'], return_connection['bags_allowed']),
                              'bags_count': self.bags_count,
                              'destination': self.origin,
                              'origin': self.destination,
                              'total_price': total_price,
                              'travel_time': travel_time}

                    self.add_to_connections(joined, total_price)

        self.handle_output()

    def handle_output(self):
        if self.output_file is None:
            print(json.dumps(self.connections, indent=4))
        else:
            with open(self.output_file, 'w') as output:
                json.dump(self.connections, output, indent=4)

    def run(self):
        if self.origin not in self.flights:
            print(f'origin city {self.origin} is not present in the input file')
            self.handle_output()
            return

        if self.is_return:
            self.search_return_flights()
        else:
            self.get_all_connections()
            self.handle_output()

    def search_for_flights(self, current: str, path=[], visited=set()):
        # print(current, 'current', visited)

        departure_time_from, departure_time_until = get_times(path)

        if current not in self.nodes:
            self.nodes[current] = Node()
        node = self.nodes[current]

        new_visited = visited | {current}

        if self.destination == current:
            yield path
        elif node.is_end:
            for way in node.ways:
                next_flight = way[-1]
                if departure_time_from <= convert_iso_to_unix(next_flight.departure) <= departure_time_until and\
                        self.check_path(way, visited):
                    yield path + way
        else:
            for flight in self.flights[current]:
                dest = flight.destination
                if dest not in visited and\
                   departure_time_from <= convert_iso_to_unix(flight.departure) <= departure_time_until:
                    new_path = path + [flight]
                    for final_path in self.search_for_flights(dest, new_path, new_visited):
                        node.ways.append(final_path)
                        yield final_path

        node.is_end = True

        return

    def check_path(self, path: List[Flight], visited: Set[str]):
        return all(map(lambda x: x not in visited and self.bags_count <= x.bags_allowed, path))

    def add_to_connections(self, connection: Connection, total_price):
        def get_price(lst, i):
            return lst[i]['total_price']
        add_to_lst(self.connections, connection, total_price, get_price)

    def get_stats_about_connection(self, path):
        bags_allowed = path[0].bags_allowed
        total_price = 0
        flights = []
        for flight in path:
            flights.append(flight.__dict__)
            bags_allowed = min(bags_allowed, flight.bags_allowed)
            total_price += flight.base_price + self.bags_count * flight.bag_price

        return flights, total_price, bags_allowed

    def get_all_connections(self):
        for path in self.search_for_flights(self.origin):

            first_flight = path[0]
            last_flight = path[-1]

            bags_allowed = first_flight.bags_allowed
            total_price = 0
            flights = []

            for flight in path:
                flights.append(flight.__dict__)
                bags_allowed = min(bags_allowed, flight.bags_allowed)
                total_price += flight.base_price + self.bags_count * flight.bag_price

            flights, total_price, bags_allowed = self.get_stats_about_connection(path)

            departure = convert_iso_to_unix(first_flight.departure)
            arrival = convert_iso_to_unix(last_flight.arrival)
            travel_time = arrival - departure

            connection = {'flights': flights,
                          'bags_allowed': bags_allowed,
                          'bags_count': self.bags_count,
                          'destination': self.destination,
                          'origin': self.origin,
                          'total_price': total_price,
                          'travel_time': convert_seconds_to_time(int(travel_time))}

            self.add_to_connections(connection, total_price)


class Node:
    def __init__(self, is_end=False) -> None:
        self.ways: List[List[Flight]] = []
        self.is_end = is_end

    def append(self, path: List[Flight]) -> None:
        def get_unix_time(lst: List[List[Flight]], i: int) -> float:
            return convert_iso_to_unix(lst[i][0].arrival)

        add_to_lst(self.ways, path, convert_iso_to_unix(path[0].departure), get_unix_time)


def add_to_lst(lst, elem, value, get_elem_value):
    low, high = 0, len(lst) - 1

    while high >= low:
        mid = (high + low) // 2
        if value < get_elem_value(lst, mid):
            high = mid - 1
        else:
            low = mid + 1

    lst.insert(low, elem)


def get_times(path):
    if path:
        unix = convert_iso_to_unix(path[-1].arrival)
        return unix + HOUR_IN_SECONDS, unix + 6 * HOUR_IN_SECONDS
    return 0, float('inf')


def convert_seconds_to_time(seconds: float) -> str:
    return str(timedelta(seconds=seconds))


def convert_time_to_seconds(time: str) -> int:
    hours, mins, secs = time.split(':', 3)
    return 3600 * int(hours) + 60 * int(mins) + int(secs)


def convert_iso_to_unix(time: str) -> Optional[float]:
    try:
        date = datetime.fromisoformat(time)
    except ValueError:
        print(f'{time} is not valid iso format')
        return None

    return datetime.timestamp(date)


def convert_unix_to_iso(seconds: float) -> str:
    return datetime.fromtimestamp(int(seconds)).isoformat()


def check_arguments(arguments: argparse.Namespace) -> bool:
    if arguments.from_time is not None and convert_iso_to_unix(arguments.from_time) is None:
        return False
    if arguments.until_time is not None and convert_iso_to_unix(arguments.until_time) is None:
        return False
    return True


def parse_arg() -> Optional[Dict[str, Union[str, int]]]:
    parser = argparse.ArgumentParser(prog='flights.py',
                                     description='Program will load data from input file ' +
                                                 'and search for suitable connections from origin to destination. ' +
                                                 'Time on intechange station cannot be lower then 1 hour ' +
                                                 'and cannot exceed 6 hours')
    parser.add_argument('input_file', help='file with database of flights ' +
                                           'could be also directory, where are stored files with database ' +
                                           'program will recursively search for csv files and load their data')
    parser.add_argument('origin', help='starting airport')
    parser.add_argument('destination', help='final airport')
    parser.add_argument('-r', '--return_flight', help='search also return flight from final destination to origin',
                        action='store_true', default=False)
    parser.add_argument('-f', '--from_time', help='will only search for flights from entered date ' +
                                       'date should be in iso format')
    parser.add_argument('-u', '--until_time', help='will only search for flights until entered date ' +
                                        'date should be in iso format')
    parser.add_argument('-o', '--output', help='available flights will be stored as JSON in file, ' +
                                                         'otherwise are flights printed on screen')
    parser.add_argument('-d', '--days', type=int, help='days of stay in final destination '
                                                       'program will search flights from interval <DAYS, DAYS + 24 hours> ')
    parser.add_argument('-b', '--bags_count', type=int, help='count of bags you take to plane', default=0)
    parser.add_argument('-l', '--load_message', action='store_true',
                        help='print message what lines was loaded', default=False)

    args = parser.parse_args()

    if not check_arguments(args):
        return None

    return vars(args)


if __name__ == '__main__':
    args = parse_arg()
    if args is not None:
        flights = Data(args['input_file']).get_flights()
        del args['input_file']
        search = Search(flights, **args)
        search.run()

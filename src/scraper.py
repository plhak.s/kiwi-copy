import requests
from datetime import datetime
from redis import Redis
from slugify import slugify
import argparse
from collections import namedtuple
import re
import json
from journeys import get_journeys, store_journeys
# from bs4 import BeautifulSoup

# fields = ('src', 'dest', 'date_from', 'dest_to', 'passengers')
# Request = namedtuple('request', fields, defaults=(None, ) * 2)
Request = namedtuple('request', 'src dest date')

DATETIME_SUPPORTED_FORMATS = ["%d-%m-%Y", "%Y-%m-%d"]
SURNAME = 'plhak'


class MyRedis:
    def __init__(self):
        self.redis = Redis(host='redis.pythonweekend.skypicker.com', decode_responses=True)

    def _loc_key(self, loc):
        return f'{SURNAME}:location:{slugify(loc)}'

    def _journey_key(self, request: Request):
        return f'{SURNAME}:journey:{slugify(request.src)}_{slugify(request.dest)}_{request.date}'

    def store_loc(self, loc, id_):
        key = self._loc_key(loc)
        self.redis.set(key, id_)

    def load_loc(self, loc):
        key = self._loc_key(loc)
        return self.redis.get(key)

    def store_journey(self, request: Request, res):
        key = self._journey_key(request)
        parsed = self._parse(res)
        self.redis.set(key, parsed)

    def _parse(self, res):
        return json.dumps(res)

    def load_journey(self, request: Request):
        # expiry =
        key = self._journey_key(request)
        res = self.redis.get(key)
        if res is None:
            return None
        try:
            res = json.loads(res)
        except Exception:
            return None

        return res


class Scraper:
    def __init__(self, request: Request):
        self.request = request
        self._city_map = None
        self.redis = MyRedis()

    def _build_city_map(self):
        req = requests.get('https://brn-ybus-pubapi.sa.cz/restapi/consts/locations')
        if req.status_code != 200:
            raise Exception('request not accepted')
        city_map = {}
        for country in req.json():
            for city in country['cities']:
                loc = city['name'].lower()
                id_ = city['id']
                city_map[loc] = id_
                self.redis.store_loc(loc, id_)
                for alias in city['aliases']:
                    loc = alias.lower()
                    self.redis.store_loc(loc, id_)
                    city_map[loc] = id_

        return city_map

    @property
    def city_map(self):
        if self._city_map is None:
            self._city_map = self._build_city_map()
        return self._city_map

    def _parse_data(self, data):
        loc, dest = self.request.src, self.request.dest
        parsed = []
        for connection in data['routes']:
            vehicle_types = ','.join([t.lower() for t in connection['vehicleTypes']])
            conn = {
                "departure": parse_iso_format(connection['departureTime']),
                "arrival": parse_iso_format(connection['arrivalTime']),
                "origin": loc,
                "destination": dest,
                "fare": {
                    "amount": float(connection['priceFrom']),
                    "currency": "EUR"
                },
                "type": vehicle_types,
                "source_id": connection['departureStationId'],
                "destination_id": connection['arrivalStationId'],
                "free_seats": connection['freeSeatsCount'],
                "carrier": "REGIOJET"
            }
            parsed.append(conn)
        return parsed

    def _get_loc_dest(self):
        loc = self.redis.load_loc(self.request.src)
        if loc is None:
            loc = self.city_map[self.request.src.lower()]
            dest = self.city_map[self.request.dest.lower()]
            return loc, dest

        dest = self.redis.load_loc(self.request.dest)
        if dest is None:
            dest = self.city_map[self.request.dest.lower()]

        return loc, dest

    def _load_data(self):
        loc_id, dest_id = self._get_loc_dest()
        tm = self.request.date
        req = requests.get(
            f'https://brn-ybus-pubapi.sa.cz/restapi/routes/search/simple?tariffs=REGULAR&toLocationType=CITY&toLocationId={dest_id}&fromLocationType=CITY&fromLocationId={loc_id}&departureDate={tm}')
        if req.status_code != 200:
            raise Exception(f'request not accepted with http code: {req.status_code}')
        return req.json()

    def _load_from_database(self):
        res = get_journeys(self.request.src, self.request.dest, self.request.date)
        print(res, '------------------')

    def _load_from_cache(self):
        return self.redis.load_journey(self.request)

    def scrape(self):
        res = self._load_from_cache()
        if res is not None:
            return res

        res = self._load_from_database()
        if res is not None:
            return res

        data = self._load_data()

        parsed = self._parse_data(data)
        self.redis.store_journey(self.request, parsed)
        store_journeys(parsed)

        return parsed


def convert_years_months_date(tm: str):
    return datetime.strptime(tm, '%Y-%m-%d')


def check_time(tm: str):
    if re.match(r'^[0-9]{4}-[0-9]{2}-[0-9]{2}', tm) is None:
        print('Time format is not in valid format, should be in %Y-%m-%d format')
        return False

    date = convert_years_months_date(tm)
    today = datetime.today()
    if date < today:
        print('Entered departure to the past')
        return False
    return True


def parse_iso_format(date: str):
    return re.split(r'\.', date)[0]


def date_type(date_string):
    for fmt in DATETIME_SUPPORTED_FORMATS:
        try:
            return datetime.strptime(date_string, fmt)
        except ValueError:
            pass

    msg = f"date {date_string} doesn't match formats {DATETIME_SUPPORTED_FORMATS}"
    raise NotImplementedError(msg)


def parse_input_request() -> Request:
    parser = argparse.ArgumentParser()

    parser.add_argument("src")
    parser.add_argument("dest")
    parser.add_argument("date", type=date_type)

    args = parser.parse_args()

    return Request(args.src, args.dest, args.date)


if __name__ == '__main__':
    request = Request('Brno', 'Praha', '2021-12-12')

    print(json.dumps(Scraper(request).scrape(), indent=4))

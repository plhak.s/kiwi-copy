from sqlalchemy import MetaData
from contextlib import contextmanager
from sqlalchemy import create_engine, Column, Integer, Numeric, Sequence, String, Table, TEXT, UniqueConstraint
from sqlalchemy.dialects.postgresql import ENUM, TIMESTAMP


metadata = MetaData()

DATABASE_URL = 'postgresql://pythonweekend:9SRK7eJG6T8rirWW@sql.pythonweekend.skypicker.com/pythonweekend'

engine = create_engine(
    DATABASE_URL,
    pool_size=1,
    max_overflow=0,
    echo=True,
)

Journeys = Table(
    "journeys_plhak",
    metadata,
    Column("id", Integer, Sequence("journeys_seq_plhak"), primary_key=True),
    Column("source", TEXT, index=True, nullable=False),
    Column("destination", TEXT, index=True, nullable=False),
    Column("departure_datetime", TIMESTAMP, nullable=False),
    Column("arrival_datetime", TIMESTAMP, nullable=False),
    Column("carrier", TEXT, index=True, nullable=False),
    Column("vehicle_type", ENUM("airplane", "bus", "train", name="vehicle_type")),
    Column("price", Numeric(20, 6), nullable=False),
    Column("currency", String(3), nullable=False),
    UniqueConstraint("source", "destination", "departure_datetime",
                     "arrival_datetime", "carrier", name="unique_journey_plhak22")
)


@contextmanager
def database_connection():
    with engine.connect() as connection:
        yield connection


def create_tables():
    metadata.create_all(engine)

from fastapi import FastAPI
from fastapi.responses import JSONResponse
from datetime import datetime
from storage.database_ import create_tables
# from journeys import combs, search_journey
from fastapi.middleware.cors import CORSMiddleware
from sqlalchemy import alias, select, and_
from storage.database_ import database_connection, Journeys
from scraper import Request, Scraper
from datetime import date, timedelta

app = FastAPI()

origins = [
    "http://localhost",
    "http://localhost:8080",
    "http://192.168.51.38:3000"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.on_event("startup")
def create_tables_on_startup():
    create_tables()


@app.get("/")
def root():
    return {"message": "Hello World"}


@app.get("/search")
def search(origin: str, destination: str, departure: datetime):
    return JSONResponse(search_journey(origin, destination, departure.date()))


@app.get("/combinations")
def combinations(source: str, destination: str, date: datetime):
    yield from combs(source, destination, date)


def combs(source: str, destination: str, date: date):
    aJourneys = alias(Journeys)
    start, end = f'{date} 00:00:00 ', f'{date} 24:00:00 '
    with database_connection() as conn:
        query = select([Journeys, aJourneys]).join(
            aJourneys, Journeys.c.destination == aJourneys.c.source
            ).where(and_(Journeys.c.source == source, aJourneys.c.destination == destination,
                         Journeys.c.departure_datetime.between(start, end),
                         aJourneys.c.departure_datetime.between(Journeys.c.arrival_datetime,
                                                                Journeys.c.arrival_datetime + timedelta(hours=6))))
        results = conn.execute(query).fetchall()

    for combination in results:
        yield {
            "source": combination["source"],
            "stopover": combination["destination"],
            "destination": combination["destination_1"],
            "departure_datetime": combination["departure_datetime"],
            "arrival_datetime": combination["arrival_datetime_1"]
        }


def search_journey(source: str, destination: str, departure_from: date):
    request = Request(source, destination, departure_from)
    journeys = Scraper(request).scrape()
    # with database_connection() as conn:
    #     for journey in journeys:
    #         create_journey(conn, journey)

    return journeys

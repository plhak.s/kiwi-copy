from typing import Optional, List
from sqlalchemy import select, and_
from sqlalchemy.dialects.postgresql import insert
from sqlalchemy.engine import Connection, Row
from storage.database_ import database_connection, Journeys


def parse_journey(journey):
    return {
        "source": journey['origin'],
        "destination": journey['destination'],
        "departure_datetime": journey['departure'],
        "arrival_datetime": journey['arrival'],
        "carrier": journey['carrier'],
        "vehicle_type": journey['type'].split(',', 1)[0],
        "price": journey['fare']['amount'],
        "currency": journey['fare']['currency']
    }


def parse_back():
    pass


def store_journeys(journeys):
    with database_connection() as conn:
        for journey in journeys:
            create_journey(conn, journey)


def create_journey(connection: Connection, journey) -> Optional[Row]:
    query = insert(Journeys).values(**parse_journey(journey)).returning("*").on_conflict_do_nothing()
    executed_query = connection.execute(query)
    return executed_query.one_or_none()


def get_journeys(src, dest, date):
    with database_connection() as conn:
        res = get_journeys_(conn, src, dest, date)
    return res


def get_journeys_(connection: Connection, src, dest, date) -> List[Row]:
    query = select(Journeys).where(and_(Journeys.c.destination == dest,
                                        Journeys.c.source == src,
                                        Journeys.c.departure_datetime == date))
    rows = connection.execute(query).all()
    return rows


# def combs(source: str, destination: str, date: date):
#     aJourneys = alias(Journeys)
#     start, end = f'{date} 00:00:00 ', f'{date} 24:00:00 '
#     with database_connection() as conn:
#         query = select([Journeys, aJourneys]).join(
#             aJourneys, Journeys.c.destination == aJourneys.c.source
#             ).where(and_(Journeys.c.source == source, aJourneys.c.destination == destination,
#                          Journeys.c.departure_datetime.between(start, end),
#                          aJourneys.c.departure_datetime.between(Journeys.c.arrival_datetime,
#                                                                 Journeys.c.arrival_datetime + timedelta(hours=6))))
#         results = conn.execute(query).fetchall()
#
#     for combination in results:
#         yield {
#             "source": combination["source"],
#             "stopover": combination["destination"],
#             "destination": combination["destination_1"],
#             "departure_datetime": combination["departure_datetime"],
#             "arrival_datetime": combination["arrival_datetime_1"]
#         }
#
#
# def search_journey(source: str, destination: str, departure_from: date):
#     request = Request(source, destination, departure_from)
#     journeys = Scraper(request).scrape()
#     # with database_connection() as conn:
#     #     for journey in journeys:
#     #         create_journey(conn, journey)
#
#     return journeys

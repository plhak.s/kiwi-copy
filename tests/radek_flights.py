from __future__ import annotations
from math import inf
from typing import Dict, Set, List, Generator, Optional
from datetime import datetime
from collections import defaultdict
from copy import copy
import csv
import json
import argparse

Airport = str


class Flight:
    def __init__(self, flight_no: str, origin_airport: Airport, destination_airport: Airport, departure: str,
                 arrival: str, base_price: float, bag_price: float, bags_allowed: int) -> None:
        self.flight_no = flight_no
        self.origin = origin_airport
        self.destination = destination_airport
        self.departure = departure
        self.arrival = arrival
        self.base_price = base_price
        self.bag_price = bag_price
        self.bags_allowed = bags_allowed

    def __str__(self) -> str:
        return f"Flight no. {self.flight_no} from {self.origin} to {self.destination} at {self.departure}"

    def check_layover_time(self, connecting_flight: Flight) -> bool:
        difference = datetime.fromisoformat(connecting_flight.departure) - datetime.fromisoformat(self.arrival)
        return difference.days == 0 and 3600 <= difference.seconds <= 21600 # 1 - 6 hours


Connections = Dict[Airport, Set[Flight]]


class ConnectionFinder:
    def __init__(self, bag_count: int, change_count: int) -> None:
        self.connections = None
        self.bag_count = bag_count
        self.changes = change_count

    def load_data(self, file_path: str):
        result: Connections = defaultdict(lambda: set())
        with open(file_path) as csv_file:
            csv_reader = csv.reader(csv_file)
            csv_reader.__next__()
            for row_number, row in enumerate(csv_reader):
                flight_no, origin, destination, departure, arrival, base_price, bag_price, bags_allowed = \
                    row[0], row[1], row[2], row[3], row[4], float(row[5]), float(row[6]), int(row[7])
                flights = result[origin]
                flights.add(
                    Flight(flight_no, origin, destination, departure, arrival, base_price, bag_price, bags_allowed))
        self.connections = result
        return result

    def find_connections(self, origin_airport: Airport, destination_airport: Airport) -> Generator[
            List[Flight], None, None]:
        for flight in self.connections[origin_airport]:
            yield from self._find_flight_connections(flight, destination_airport)

    def _find_flight_connections(self, flight: Flight, wanted_destination: Airport,
                                 journey: Optional[List[Flight]] = None,
                                 visited: Optional[Set[Airport]] = None) -> Generator[List[Flight], None, None]:

        if journey is None:
            journey = []
            visited = set()
        journey.append(flight)
        visited.add(flight.origin)

        if flight.destination == wanted_destination and flight.bags_allowed >= self.bag_count:
            yield journey
        elif len(journey) <= self.changes:
            for connecting_flight in self.connections[flight.destination]:
                if connecting_flight.destination not in visited and flight.check_layover_time(connecting_flight) \
                        and connecting_flight.bags_allowed >= self.bag_count:
                    yield from self._find_flight_connections(connecting_flight, wanted_destination, journey, visited)
        journey.pop()
        visited.remove(flight.origin)  # type: ignore


def total_price(journey: List[Flight], bag_count: int = 0) -> float:
    total: float = 0
    for flight in journey:
        total += flight.base_price + flight.bag_price * bag_count
    return total


def travel_time(journey: List[Flight]) -> str:
    first_flight = journey[0]
    last_flight = journey[-1]
    return str(datetime.fromisoformat(last_flight.arrival) - datetime.fromisoformat(first_flight.departure))


def bags_allowed(journey: List[Flight]) -> int:
    allowance = inf
    for flight in journey:
        if flight.bags_allowed < allowance:
            allowance = flight.bags_allowed
    return allowance  # type: ignore


def max_changes(connection, destination):
    changes = 0
    for flight in connection:
        if flight.destination == destination:
            break
        changes += 1
    return changes if len(connection) - 1 == changes else max(changes, len(connection) - changes - 1)


def prettify_route(connection, bags, origin, destination, returning):
    connection_record = {
        "flights": [
            flight.__dict__ for flight in connection
        ],
        "bags_allowed": bags_allowed(connection),
        "bags_count": bags,
        "destination": destination,
        "origin": origin,
        "total_price": total_price(connection),
        "travel_time": travel_time(connection)
        # "max_changes": max_changes(connection, destination),
        # "returning": returning
    }
    return connection_record


def join_returning_flights(routes_to_destination, routes_from_destination):
    joined_connections = []
    routes_from_destination.sort(key=lambda route: route[0].departure, reverse=True)
    for connection_to in routes_to_destination:
        for connection_from in routes_from_destination:
            if connection_to[-1].arrival < connection_from[0].departure:
                joined_connections.append(connection_to + connection_from)
            else:
                break
    return joined_connections


def save_finding(connections, output):
    with open(output, "w") as output_file:
        json.dump(connections, output_file, indent=4)


def run(args):
    finder = ConnectionFinder(args.bags, args.changes)
    finder.load_data(args.data_path)

    found_connections = [copy(connection) for connection in finder.find_connections(args.origin, args.destination)]
    if args.returning:
        return_connections = [copy(connection) for connection in finder.find_connections(args.destination, args.origin)]
        found_connections = join_returning_flights(found_connections, return_connections)

    found_connections = [prettify_route(connection, args.bags, args.origin, args.destination, args.returning) for connection in found_connections]
    found_connections.sort(key=lambda route: route["total_price"])
    json_connections = json.dumps(found_connections, indent=4)
    print(json_connections)
    # print(f"CONNECTIONS FOUND: {len(found_connections)}")
    if args.output != "":
        save_finding(found_connections, args.output)
    return json_connections


def argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("data_path", type=str)
    parser.add_argument("origin", type=str)
    parser.add_argument("destination", type=str)
    parser.add_argument("--bags", "-b", type=int, default=0, nargs="?", choices=range(0, 100), metavar='0-99')
    parser.add_argument("--changes", "-ch", type=int, default=inf, nargs="?", choices=range(0, 100), metavar='0-99')
    parser.add_argument("--returning", "-r", action='store_true')
    parser.add_argument("--output", "-o", nargs="?", default="")

    args = parser.parse_args()
    print(args)
    return args


if __name__ == "__main__":
    arguments = argument_parser()
    run(arguments)

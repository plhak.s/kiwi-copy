from flights import Search, add_to_lst
from radek_flights import ConnectionFinder, run, argument_parser
import csv
import json
import io
from contextlib import redirect_stdout
from itertools import combinations
import argparse

def get_all_cities(filename):
    cities = set()
    with open(filename, 'r') as f:
        reader = csv.DictReader(f)
        for line in reader:
            cities.add(line['origin'])
            cities.add(line['destination'])
    return cities


def test_file(filename):
    cities = get_all_cities(filename)
    comb = combinations(cities, 2)
    for origin, dest in comb:
        test_from_to(filename, origin, dest)
        # f = io.StringIO()
        # with redirect_stdout(f):
        #     my_search = Search(filename, origin, dest, 0, False, None, None, None, None, False)
        #     my_search.run()
        # my_out = f.getvalue()
        # my_json = json.loads(my_out)
        #
        # g = io.StringIO()
        # with redirect_stdout(g):
        #     ns = argparse.Namespace(data_path=filename, origin=origin,
        #                             destination=dest, bags=0, changes=float('inf'),
        #                             returning=False, output='')
        #     run(ns)
        #
        # radek_out = g.getvalue()
        # radek_json = json.loads(radek_out)
        # if len(my_json) == 0:
        #     if len(radek_out) == 0:
        #         print(f'for file: {filename}, origin: {origin} and destination: {dest} => True')
        #     else:
        #         print(f'for file: {filename}, origin: {origin} and destination: {dest} => False')
        # else:
        #     print(f'for file: {filename}, origin: {origin} and destination: {dest} =>' +
        #           f'{sorted(my_json[0].items()) == sorted(radek_json[0].items())}')
            # print(f'for file: {filename}, origin: {origin} and destination: {dest} =>' +
            #       f'{cmp_jsons(my_json, radek_json)}')


def test():
    files = ['example0.csv', 'example1.csv', 'example2.csv', 'example3.csv']
    for file in files:
        test_file(file)


def search(connection, second_json):
    for conn in second_json:
        A = conn['departure']
        if sorted(conn.items()) == sorted(connection.items()):
            if sorted(conn['flights'].items()) == sorted(connection['flights'].items()):
                return True
    return False


def cmp_jsons(first_json, second_json):
    for o_connection in first_json:
        if not search(o_connection, second_json):
            return False
    return True


def test_from_to(filename, origin, dest):

    # my_search = Search(filename, origin, dest, 0, False, None, None, None, None, False)
    # my_search.run()
    # # my_json = json.dumps(my_out, sort_keys=True)
    #
    # ns = argparse.Namespace(data_path=filename, origin=origin,
    #                             destination=dest, bags=0, changes=float('inf'),
    #                             returning=False, output='')
    # run(ns)

    f = io.StringIO()
    with redirect_stdout(f):
        my_search = Search(filename, origin, dest, 0, False, None, None, None, None, False)
        my_search.run()
    my_out = f.getvalue()
    my_json = json.loads(my_out)

    # print(origin, dest)
    g = io.StringIO()
    with redirect_stdout(g):
        ns = argparse.Namespace(data_path=filename, origin=origin,
                                destination=dest, bags=0, changes=float('inf'),
                                returning=False, output='')
        run(ns)

    radek_out = g.getvalue()
    radek_json = json.loads(radek_out)
    radek_json.sort(key=lambda route: route['flights'][0]['departure'])
    x = []
    # for conn in radek_json[0]:


    print(f'for file: {filename}, origin: {origin} and destination: {dest} => {sorted(my_json[0].items()) == sorted(radek_json[0].items())}')
    # print(f'for file: {filename}, origin: {origin} and destination: {dest} => {cmp_jsons(my_json, radek_json)}')

    for conn in my_json:
        print(conn['flights'][0]['flight_no'])

    print('------------------------')
    for conn in radek_json:
        print(conn['flights'][0]['flight_no'])
    # print(radek_out)


if __name__ == '__main__':
    test_from_to('example1.csv', 'SML', 'NRX')
    # test()